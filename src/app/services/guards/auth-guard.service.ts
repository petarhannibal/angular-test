import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {

    let loggedIn = false; // replace with actual user auth checking logic
    let token = localStorage.getItem('accessToken');

    if(token)
      loggedIn = true;

    if (!loggedIn) {
      this.router.navigate(['/login']);
    }

    return loggedIn;
  }
}