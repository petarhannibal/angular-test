import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse,
    HttpClient
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root'
})
export class TokenIntereceptorService {

    constructor(
        private router: Router,
        private http: HttpClient
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let clonedReq = this.addHeaders(request);

        return next.handle(clonedReq).pipe(
            tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // do stuff with response if you want
                }
            }, (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401 || err.status === 403) {
                        localStorage.removeItem('accessToken');
                        this.router.navigateByUrl('/login');
                    }  else if(err.status ===500 || err.status ===400 || err.status===404)
                        {  
                            Swal.fire(
                                'GRESKA',
                                err.error.error,
                                'error'
                            )
                        }
                }
            }));
    }

    addHeaders(request) {
        const token = localStorage.getItem('accessToken');
        if (token) {
            request = request.clone({
                setHeaders: {
                    'Authorization': 'Bearer ' + token
                }
            });
        }

        if (!request.headers.has('Content-Type')) {
            request = request.clone({
                setHeaders: {
                    'Content-Type': 'application/json'
                }
            });
        }

        request = request.clone({
            headers: request.headers.set('Accept', 'application/json, text/plain, */*')
        });

        return request;
    }
}
