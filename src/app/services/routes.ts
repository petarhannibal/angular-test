export const URL = 'https://jsonplaceholder.typicode.com/posts';
export const post_url = 'https://jsonplaceholder.typicode.com/post/add';
export const delete_url = 'https://jsonplaceholder.typicode.com/post/delete';
export const update_url = 'https://jsonplaceholder.typicode.com/posts';