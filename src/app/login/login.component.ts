import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username="";
  password="";
  token="";
  tok='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0IiwibmFtZSI6IkRyYWdhbiBLcml2b2t1Y2EiLCJpYXQiOjE1MTYyMzkwMjJ9.UapdtebeJCRn8Lo6lHJHRDNzg14uyigXuoFVzdeJnWk';
  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  login(){
    let data={
      user:this.username,
      pass:this.password
    };
    if( data.user==='admin' && data.user==='admin'){
      this.token=this.tok;
      Swal.fire(
        'WELCOME',
        'ADMIN',
        'success'
    );
    this.router.navigate(['/pages']);
    }else{

Swal.fire(
  'LOGIN FAILD',
  'Bad Username or Password',
  'error'
);

    };
    localStorage.setItem('accessToken',this.token);
  }

}
