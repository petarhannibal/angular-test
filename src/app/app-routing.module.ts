import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PagesComponent } from './pages/pages.component'
import { AuthGuardGuard } from '../app/services/guards/auth-guard.service';
import { LoginGuardGuard } from '../app/services/guards/loggedin-guard.service';
const routes: Routes = [
  {
    path:'',
    redirectTo: '/login', pathMatch: 'full' 
   },
   
   {
    path:'login',canActivate: [LoginGuardGuard],
    component:LoginComponent
  },
  {
    path:'pages',canActivate: [AuthGuardGuard],
    component:PagesComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
